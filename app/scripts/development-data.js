// Default Invodo afiliate config
window.INVODO_AFF_CONFIG = {
  'test': 'true',
  'rtmpBase': 'rtmp://aoaef.invodo.com/',
  'imageBase': 'http://e.invodo.com/media/',
  'httpBase': 'http://aoael.invodo.com/media/',
  'name': 'Marsha',
  'playbuttoncolor': '',
  'aspectratio': 'WIDESCREEN',
  'defaultquality': '',
  'bufferscreenbranding': 'INVODO',
  'endofcontentbranding': 'INVODO',
  'showcliptitles': 'false',
  'ratingenabled': 'true',
  'autoplaymultipleclips': 'true',
  'playlist': 'false',
  'watermark': 'false',
  'backcolor': '000000',
  'frontcolor': 'FFFFFF',
  'secondarycolor': 'c1267d',
  'share': 'true',
  'og': 'true'
};

// Set the Invodo affiliate
window.INVODO_AFF_CONFIG.affiliate = 'gopro.com';

// Create a local IVP config for testing
window.IVP_DATA = {
  "video": {
    "autoplay": false,
    "duration": 600,
    "dimensions": {
      "width": 400,
      "height": 600,
      "unit": "px"
    }
  },
  "hotspot": {
    "enable": true,
    "templates": [
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/ivp-experiences/gopro/skiing/images/Rollbar_Mount_Thumbnanil_170x170%20copy.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "563287194848060900957c2b",
        "id": 6
      },
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/ivp-experiences/gopro/skiing/images/Antifog_Insert_Thumbnail_170x170%20copy.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "563287014848060900957c2a",
        "id": 1
      },
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/ivp-experiences/gopro/skiing/images/Chesty_Thumbnail_170x170%20copy.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "563286bd4848060900957c29",
        "id": 2
      },
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/ivp-experiences/gopro/skiing/images/AdhesiveMounts_Thumbnail_170x170%20copy.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "563286994848060900957c28",
        "id": 3
      },
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/ivp-experiences/gopro/skiing/images/HERO4_Black_Thumbnail_170x170%20copy.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "563286664848060900957c27",
        "id": 4
      },
      {
        "text": "<span class=\"ivp-hotspot-icon\"></span>",
        "_id": "55f1a905dda55c0900d0eefe",
        "id": 5
      }
    ],
    "items": [
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 4
          }
        ],
        "templateId": "563286664848060900957c27",
        "template": 4,
        "data": {
          "title": "HERO4_1Notif",
          "text": "HERO4 Black"
        },
        "time": {
          "start": 11,
          "end": 16
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563288ee4848060900957c2c",
        "$$hashKey": "032",
        "id": 0,
        "title": "HERO4_1Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 4
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "HERO4_1Beac",
          "text": "HERO4 Black"
        },
        "time": {
          "start": 11,
          "end": 13
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 52.7047913446677,
              "y": 40.65934065934066
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563288ee4848060900957c2c",
        "$$hashKey": "033",
        "id": 1,
        "title": "HERO4_1Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 3
          }
        ],
        "templateId": "563286994848060900957c28",
        "template": 3,
        "data": {
          "title": "AdMounts_Notif",
          "text": "Curved + Flat Adhesive Mounts"
        },
        "time": {
          "start": 14,
          "end": 19
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289644848060900957c33",
        "$$hashKey": "034",
        "id": 2,
        "title": "AdMounts_Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 3
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "AdMounts_Beac",
          "text": "Curved + Flat Adhesive Mounts"
        },
        "time": {
          "start": 14,
          "end": 17
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 52.39567233384853,
              "y": 22.802197802197803
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289644848060900957c33",
        "$$hashKey": "035",
        "id": 3,
        "title": "AdMounts_Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "563286bd4848060900957c29",
        "template": 2,
        "data": {
          "title": "Chesty_1Notif",
          "text": "Chesty (Chest Harness)"
        },
        "time": {
          "start": 25,
          "end": 31
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289984848060900957c3a",
        "$$hashKey": "036",
        "id": 4,
        "title": "Chesty_1Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "Chesty_1Beac",
          "text": "Chesty (Chest Harness)"
        },
        "time": {
          "start": 25,
          "end": 27
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 28.902627511591966,
              "y": 2.74725274725275
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289984848060900957c3a",
        "$$hashKey": "037",
        "id": 5,
        "title": "Chesty_1Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 1
          }
        ],
        "templateId": "563287014848060900957c2a",
        "template": 1,
        "data": {
          "title": "AntiFog_Notif",
          "text": "Anti-Fog Inserts"
        },
        "time": {
          "start": 34,
          "end": 40
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289d84848060900957c41",
        "$$hashKey": "038",
        "id": 6,
        "title": "AntiFog_Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 1
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "AntiFog_Beac",
          "text": "Anti-Fog Inserts"
        },
        "time": {
          "start": 34,
          "end": 37
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 32.61205564142195,
              "y": 3.021978021978022
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289d84848060900957c41",
        "$$hashKey": "039",
        "id": 7,
        "title": "AntiFog_Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 4
          }
        ],
        "templateId": "563286664848060900957c27",
        "template": 4,
        "data": {
          "title": "HERO4_2Notif",
          "text": "HERO4 Black"
        },
        "time": {
          "start": 34,
          "end": 40
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563288ee4848060900957c2c",
        "$$hashKey": "03A",
        "id": 8,
        "title": "HERO4_2Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 4
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "HERO4_2Beac",
          "text": "HERO4 Black"
        },
        "time": {
          "start": 34,
          "end": 37
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 37.5579598145286,
              "y": 3.021978021978022
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563288ee4848060900957c2c",
        "$$hashKey": "03B",
        "id": 9,
        "title": "HERO4_2Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "563286bd4848060900957c29",
        "template": 2,
        "data": {
          "title": "Chesty_2Notif",
          "text": "Chesty (Chest Harness)"
        },
        "time": {
          "start": 47,
          "end": 53
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289984848060900957c3a",
        "$$hashKey": "03C",
        "id": 10,
        "title": "Chesty_2Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "Chesty_2Beac",
          "text": "Chesty (Chest Harness)"
        },
        "time": {
          "start": 47,
          "end": 51
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 30.757341576506953,
              "y": 34.61538461538461
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "563289984848060900957c3a",
        "$$hashKey": "03D",
        "id": 11,
        "title": "Chesty_2Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 0
          }
        ],
        "templateId": "563287194848060900957c2b",
        "template": 6,
        "data": {
          "title": "BarMount_1Notif",
          "text": "Handlebar / Seatpost / Pole Mount"
        },
        "time": {
          "start": 56,
          "end": 63
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "56328a0c4848060900957c48",
        "$$hashKey": "03E",
        "id": 12,
        "title": "BarMount_1Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 0
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "BarMount_1Beac",
          "text": "Handlebar / Seatpost / Pole Mount"
        },
        "time": {
          "start": 56,
          "end": 59
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 46.67697063369397,
              "y": 51.373626373626365
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "56328a0c4848060900957c48",
        "$$hashKey": "03F",
        "id": 13,
        "title": "BarMount_1Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 0
          }
        ],
        "templateId": "563287194848060900957c2b",
        "template": 6,
        "data": {
          "title": "BarMount_2Notif",
          "text": "Handlebar / Seatpost / Pole Mount"
        },
        "time": {
          "start": 77,
          "end": 81
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "56328a0c4848060900957c48",
        "$$hashKey": "03G",
        "id": 14,
        "title": "BarMount_2Notif",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 0
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 5,
        "data": {
          "title": "BarMount_2Beac",
          "text": "Handlebar / Seatpost / Pole Mount"
        },
        "time": {
          "start": 77,
          "end": 79
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 24.57496136012365,
              "y": 28.57142857142857
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "56328a0c4848060900957c48",
        "$$hashKey": "03H",
        "id": 15,
        "title": "BarMount_2Beac"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 0
          }
        ],
        "templateId": "563286664848060900957c27",
        "template": 4,
        "data": {
          "title": "EndCardHolder",
          "text": "."
        },
        "time": {
          "start": 600,
          "end": 601
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.5,
            "end": 0.25
          },
          "from": {
            "y": 10,
            "opacity": 0,
            "scale": 0.85
          },
          "config": [
            {
              "y": 0,
              "opacity": 1,
              "scale": 1
            },
            {
              "opacity": 0,
              "y": 10
            }
          ]
        },
        "card": "56328a374848060900957c4f",
        "$$hashKey": "1OO",
        "id": 16,
        "title": "EndCardHolder",
        "type": "notify"
      }
    ]
  },
  "card": {
    "enabled": true,
    "templates": [
      {
        "text": "<div class=\"ivp-card--large\">\n  <div class=\"ivp-card-content\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-8 ivp-card-text\">\n          <h3>${product}</h3>\n          <hr>\n          <p class=\"ivp-gopro-card-description\">${description}</p>\n          <p class=\"ivp-gopro-card-compat\">${compat}</p>\n        </div>\n        <div class=\"ivp-col-4 ivp-card-image\">\n          <img src=\"${image}\">\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"ivp-gopro-card-footer\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-6\">\n          <h3 class=\"ivp-gopro-bundle-name\">${bundle}</h3>\n          <p class=\"ivp-gopro-card-price\">${price}</p>\n          <div class=\"ivp-gopro-cart-status\">\n            <span class=\"ivp-gopro-cart-icon\"></span> Added to Cart</div>\n        </div>\n        <div class=\"ivp-col-6\">\n          <div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"#\" target=\"_blank\">Add Bundle To Cart</a></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>",
        "_id": "560b0462bcd8cb090051c29b",
        "id": 1
      },
      {
        "text": "<div class=\"ivp-card--large\">\n  <div class=\"ivp-card-content\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-12 ivp-end-card-text\">\n          <h3>Included in this Bundle</h3>\n        </div>\n        <div class=\"ivp-col-12 ivp-end-card-image\">\n          <img src=\"${image}\">\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"ivp-gopro-card-footer\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-6\">\n          <h3 class=\"ivp-gopro-bundle-name\">${bundle}</h3>\n          <p class=\"ivp-gopro-card-price\">${price}</p>\n        </div>\n        <div class=\"ivp-col-6\">\n          <div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"#\" target=\"_blank\">Add Bundle To Cart</a></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>",
        "_id": "560b048abcd8cb090051c29c",
        "id": 2
      }
    ],
    "items": [
      {
        "_id": "56328a374848060900957c4f",
        "title": "GoPro_Ski_End",
        "template": 2,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/gopro/skiing/images/bundle-summary-ski.jpg",
          "bundle": "SKI BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 5,
        "position": {
          "x":"-50%",
          "y":"6%"
        }
      },
      {
        "_id": "56328a0c4848060900957c48",
        "title": "GoPro_Ski_BarMount",
        "template": 1,
        "data": {
          "product": "Handlebar / Seatpost / Pole Mount",
          "description": "Mount your GoPro to handlebars, seatposts, ski poles and more.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/ivp-experiences/gopro/skiing/images/Rollbar_Mount_Thumbnanil_170x170%20copy.jpg",
          "bundle": "SKI BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 0,
        "position": {
          "x":"-50%",
          "y":"6%"
        }
      },
      {
        "_id": "563289d84848060900957c41",
        "title": "GoPro_Ski_AntiFog",
        "template": 1,
        "data": {
          "product": "Anti-Fog Inserts",
          "description": "Prevent lens fog in cold and humid environments.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/ivp-experiences/gopro/skiing/images/Antifog_Insert_Thumbnail_170x170%20copy.jpg",
          "bundle": "SKI BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 1,
        "position": {
          "x":"-50%",
          "y":"6%"
        }
      },
      {
        "_id": "563289984848060900957c3a",
        "title": "GoPro_Ski_Chesty",
        "template": 1,
        "data": {
          "product": "Chesty (Chest Harness)",
          "description": "Capture ultra immersive footage from your chest.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/ivp-experiences/gopro/skiing/images/Chesty_Thumbnail_170x170%20copy.jpg",
          "bundle": "SKI BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 2,
        "position": {
          "x":"-50%",
          "y":"6%"
        }
      },
      {
        "_id": "563289644848060900957c33",
        "title": "GoPro_Ski_AdhesiveMounts",
        "template": 1,
        "data": {
          "product": "Curved + Flat Adhesive Mounts",
          "description": "Mount your GoPro to curved and flat surfaces.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/ivp-experiences/gopro/skiing/images/AdhesiveMounts_Thumbnail_170x170%20copy.jpg",
          "bundle": "SKI BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 3,
        "position": {
          "x":"-50%",
          "y":"6%"
        }
      },
      {
        "_id": "563288ee4848060900957c2c",
        "title": "GoPro_Ski_Hero4",
        "template": 1,
        "data": {
          "product": "HERO4 Black",
          "description": "Pro-quality capture. Simply the best.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/ivp-experiences/gopro/skiing/images/HERO4_Black_Thumbnail_170x170%20copy.jpg",
          "bundle": "SKI BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 4,
        "position": {
          "x":"-50%",
          "y":"6%"
        }
      }
    ]
  },
  "panels":{
     "enable":true,
     "templates":[

     ],
     "items":[

     ]
  },
  "poster":{
     "templates":[
        {
           "id":1,
           "text":"<div class=\"ivp-gopro-header-container\"> <h2>CAPTURE YOUR SKIING MOMENTS</h2> </div> <div class=\"ivp-gopro-play-box\"> <div class=\"ivp-gopro-play-box-content\"> <p>Watch Video</p><div class=\"ivp-gopro-poster-play\"></div> </div> </div>"
        }
     ],
     "items":[
        {
           "id":1,
           "enabled":true,
           "template":1,
           "data":{

           }
        }
     ]
  },
  "endscreen":{
     "templates":[
        {
           "id":1,
           "text":"<div class=\"ivp-gopro-header-container\"> <h2>Capture Your Skiing Moments</h2> </div> <div class=\"ivp-gopro-play-box\"> <div class=\"ivp-gopro-play-box-content\"> <img class=\"ivp-gopro-poster-replay\" src=\"//ixd.invodo.com/ivp-experiences/gopro/skiing/images/replay.png\"/><p>Replay Video</p> </div> </div>"
        }
     ],
     "items":[
        {
           "id":1,
           "enabled":true,
           "template":1,
           "data":{

           }
        }
     ]
  }
}
;
